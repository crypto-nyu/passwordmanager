# Include the Dropbox SDK libraries
from dropbox import client, rest, session
import json

defaults = json.loads(open('secrets/config.json').read())

# Get your app key and secret from the Dropbox developer website
APP_KEY = defaults["APP_KEY"]
APP_SECRET = defaults["APP_SECRET"]
 
# ACCESS_TYPE should be 'dropbox' or 'app_folder' as configured for your app
ACCESS_TYPE = defaults["ACCESS_TYPE"]
 
sess = session.DropboxSession(APP_KEY, APP_SECRET, ACCESS_TYPE)
 
# We will use the OAuth token we generated already. The set_token API 
# accepts the oauth_token and oauth_token_secret as inputs.
sess.set_token(defaults["oauth_token"], defaults["oauth_token_secret"])
 
# Create an instance of the dropbox client for the session. This is
# all we need to perform other actions
client = client.DropboxClient(sess)
def dropbox(user_option):
    if user_option == "U":

    # Let's upload a file!
        try:
            filetoupload = raw_input("Enter file name with proper file extension (e.g. test.txt) \n")
            f = open(filetoupload)
            response = client.put_file(filetoupload, f, overwrite=True)
            print "uploaded:", response
        except:
            print "File not found."

    elif user_option == "D":

        try:
            folder_metadata = client.metadata('/')
            print 'metadata: ', folder_metadata

            filetodownload = raw_input("Enter file name with proper file extension (e.g. test.txt) \n")
            f, metadata = client.get_file_and_metadata(filetodownload)
            out = open(filetodownload, 'wb')
            out.write(f.read())
            out.close()
            print metadata
    
        except:
            print "File not found."

    else:
        print "Invalid option!"
        
