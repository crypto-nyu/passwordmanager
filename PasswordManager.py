#!usr/bin/env python
import sys
import random
import os
import passwordmeter
import base64
import binascii
import json
import hashlib
import clipboard
import dropbox_uploaddownloadFile
from Crypto import Random
from Crypto.Cipher import AES
from Crypto.Util import Counter
from Crypto.Random import get_random_bytes
from aesWrapper import AESCipher

defaults = json.loads(open('secrets/config.json').read())

BS = 16
pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS)
unpad = lambda s : s[0:-ord(s[-1])]
alphabets = "abcdefghijklmnopqrstuvwxyz"
special_chars = "^!\$%&/()=?{[]}+~#-_.:,;<>|\\'"
upperalphabets = alphabets.upper()


hash_master = hashlib.sha256(defaults["MASTER"]).digest()
aes = AESCipher(hash_master[:128])
#aes = AESCipher("&7RsoJ[eU!>Zd\880reE9)P9")

#nonce = get_random_bytes(8)

def generate_credentials(enc_mode):
    print "--------------------------------------------------------------------"
    print "Please generate login credentials "
    print "--------------------------------------------------------------------"
    username = raw_input("Enter Username: \n")
    print "--------------------------------------------------------------------"
    print "Do you want to generate password using this app?"
    print "--------------------------------------------------------------------"
    password_response = raw_input("Enter Y: Yes or N: NO \n")
    print "--------------------------------------------------------------------"
    if password_response == "Y":
        length = input("Enter the length of the password \n")
        if length < 6:
            length = input ("Password is too short. Please enter the new length greater than or equal to 7: \n")
            print "--------------------------------------------------------------------"
            password = password_gen(length)
        else:
            password = password_gen(length)
        check_passwordstrength(password)
        find_duplicates(enc_mode, username, password)
    elif password_response == "N":
        password = raw_input("Enter Password: \n")
        check_passwordstrength(password)
        print "--------------------------------------------------------------------"
        find_duplicates(enc_mode, username, password)
    else:
        print "Invalid option!"


def find_duplicates(enc_mode, username, password):
    with open("./passfile.txt", "r+") as pass_file:
        passfile = "./passfile.txt"
        if os.stat("./passfile.txt").st_size == 0:
            if enc_mode == "1":
                delete_credentials(username)
                store_credentials(enc_mode, username, password, passfile)
        else:
            lines = pass_file.readlines()
            for line in lines:
                line = line.split(":")
                #encrypted = line[1].strip()
                if line[0] == username and aes.decrypt(line[1].strip()) == password:
                    print "This pair of username and passoword already exists!"
                    print "--------------------------------------------------------------------"
                    option = raw_input("Do you want to generate new credentials? Y/N \n")
                    if option == "Y":
                        delete_credentials(username)
                        generate_credentials(enc_mode)
                        break
                    elif option == "N":
                        print "Okay"
                    else:
                        print "Invalid option!"
                elif line[0] == username and aes.decrypt(line[1].strip()) != password:
                    print "This username already exixts!"
                    print "--------------------------------------------------------------------"
                    option = raw_input("Do you want to store new password for this username? Y/N \n")
                    if option == "Y":
                        if enc_mode == "1":
                            delete_credentials(username)
                            store_credentials(enc_mode, username, password, passfile)
                            break
                        else:
                            break
                    elif option == "N":
                        print "Okay"
                    else:
                        print "Invalid option!"
                else:
                    if enc_mode == "1":
                        store_credentials(enc_mode, username, password, passfile)
                        break
                    else:
                        break
    
            
    

def store_credentials(enc_mode, username, password, passfile):
    iv = Random.new().read(AES.block_size)
    with open(passfile, "a") as pass_file:
        pass_file.write(username + ":" + aes.encrypt(iv+password) + "\n")
        


def get_password():
    found = False
    username = raw_input("Enter username for which password is required: \n")
    print "--------------------------------------------------------------------"
    with open("./passfile.txt", "r") as pass_file1:
        lines = pass_file1.readlines()
        for line in lines:
            if username in line:
                line = line.split(":")
                password = line[1].strip()
                enc_mode = "1"
                password = aes.decrypt(password)
                found = True
                u1 = raw_input("Do you want to display or copy to clipboard? D/C \n")
                if u1 == "D":
                    print password
                elif u1 == "C":
                    clipboard.copy(password)
                    print "Password is copied to clipboard"
                else:
                    print "Invalid option"
    return found

   

def delete_credentials(username):
    found = False
    readfile1 = open ("./passfile.txt", "r")
    tmp = []
    for line in readfile1:
        var = line.split(":")
        if var[0] == username:
            found = True
            continue
        else:
            tmp.append(line)
    readfile1.close()
    writefile1 = open("./passfile.txt", "w")
    writefile1.writelines(tmp)
    writefile1.close()
    return found


def password_gen(length):
    password_list = []
    for i in range(length//4):
        password_list.append(alphabets[random.randrange(len(alphabets))])
        password_list.append(upperalphabets[random.randrange(len(upperalphabets))])
        password_list.append(str(random.randrange(10)))
        password_list.append(special_chars[random.randrange(len(special_chars))])

    for i in range(length-len(password_list)):
         password_list.append(alphabets[random.randrange(len(alphabets))])
        # password_list.append(special_chars[random.randrange(len(special_chars))])

    random.shuffle(password_list)

    password = "".join(password_list)
    
    return password

def check_passwordstrength(password):
    strength, improvements = passwordmeter.test(password)
    if (strength < 0.8):
        print "Password Strength: Poor"
    elif (strength > 0.8) and (strength < 0.9):
        print "--------------------------------------------------------------------"
        print "\n" + password
        print "Password Strength: Weak"
        print "--------------------------------------------------------------------"
    elif (strength > 0.9) and (strength < 0.95):
        print "--------------------------------------------------------------------"
        print "\n" + password
        print "Password Strength: Moderate"
    elif (strength > 0.95) and (strength < 0.98):
        print "--------------------------------------------------------------------"
        print "\n" + password
        print "Password Strength: Strong"
        print "--------------------------------------------------------------------"
    else :
        print "--------------------------------------------------------------------"
        print "\n" + password
        print "Password Strength: Very Strong"
        print "--------------------------------------------------------------------"

def main():
    print "####################################################################"
    print "#               Welcome to Password Manager                        #"
    print "####################################################################"
    print "\n" + "____________NOTE: ALL USER INPUTS ARE CASE-SENSITIVE_____________" +"\n"
    open('./passfile.txt','a')
    count = 0
    while(True):
        master_input = raw_input("Enter the passcode: \n")
        print "--------------------------------------------------------------------"
        master = hashlib.sha256(master_input).digest()
        if master == hash_master:
            menu = raw_input("Menu:" + "\n" + "1: Read a password from file" + "\n" + "2: Write a password into file" + "\n" + "3: Delete a password from file" + "\n" +  "4: Upload/Download to Dropbox"+ "\n" +"5: Exit \n")
            print "--------------------------------------------------------------------"
            if menu == "1":
                pw = get_password()
                if pw == True:
                    continue
                else:
                    print "------------------------Username is not in the file------------------------------"
                r_option = raw_input("Do you want to continue? Y/N \n")
                if r_option == "Y":
                    continue
                else:
                    sys.exit(0)
              
            elif menu == "2":
               enc_mode = "1"
               print "--------------------------------------------------------------------"
               generate_credentials(enc_mode)
               print "-----------------------Success:Password stored into the file--------------------"
               w_option = raw_input("Do you want to continue? Y/N \n")
               if w_option == "Y":
                   continue
               else:
                   sys.exit(0)
            elif menu == "3":
                username = raw_input("Enter username which needs to be deleted: \n")
                print "--------------------------------------------------------------------"
                pw = delete_credentials(username)
                if pw == True:
                    print "-----------------------Success:Password deleted from the file--------------------"
                else:
                    print "------------------------Username is not in the file------------------------------"
                d_option = raw_input("Do you want to continue? Y/N \n")
                if d_option == "Y":
                    continue
                else:
                    sys.exit(0)
            elif menu == "4":
                user_option = raw_input("Do you want to Upload or Download file to Dropbox? (U/D)")
                dropbox_uploaddownloadFile.dropbox(user_option)
                print "-------------------------**Success**---------------------------------"
                d_option = raw_input("Do you want to continue? Y/N \n")
                if d_option == "Y":
                    continue
                else:
                    sys.exit(0)
            elif menu == "5":
                sys.exit(0)
            else:
                print "Invalid option!"
                sys.exit(0)
        else:
            if(count < 5):
                count = count + 1
                print "Wrong passcode!" + "\n" + "Try Again!"
                print "--------------------------------------------------------------------"
                continue
            else:
                print "You exceeded the maximum number of attempts!"
                sys.exit(0)

if __name__=="__main__":
    main()
                
               
                

                

                    
            
        
        
            

        
                

                
                
        
    
    
    

