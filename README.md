# # README # #

# Modules to be implemented #
- Password Generator (Python)
- File Encryption and Decryption (C/C++)
- Upload and Download from cloud (Python)


# Options for User: #
1. Generate a password
   * Length
   * Uppercase
   * Lowercase
   * Special Characters
2. Upload your password file
3. Get password
   * Offline
   * Cloud