#include <iostream>
#include <iomanip>
#include <crypto++/aes.h>
#include <crypto++/modes.h>
#include <crypto++/filters.h>

using namespace std;

class AESCipher{

		char *user_key;
		byte key[ CryptoPP::AES::DEFAULT_KEYLENGTH ], iv[ CryptoPP::AES::BLOCKSIZE ];
		
		void printCiphertext(char *ciphertext){
			//
			// Dump Cipher Text
			//
			cout << "Cipher Text (" << strlen(ciphertext) << " bytes)" << endl;

			for( int i = 0; i < strlen(ciphertext); i++ ) {
				cout << "0x" << hex << (0xFF & static_cast<byte>(ciphertext[i])) << " ";
			}
			cout << endl << endl;
		}

	public:

		AESCipher(char* user_key) {
			// Initialize key
			//Key and IV setup
			//AES encryption uses a secret key of a variable length (128-bit, 196-bit or 256-   
			//bit). This key is secretly exchanged between two parties before communication   
			//begins. DEFAULT_KEYLENGTH= 16 bytes
			memset( this->key, 0x00, CryptoPP::AES::DEFAULT_KEYLENGTH );
			memset( this->iv, 0x00, CryptoPP::AES::BLOCKSIZE );

			// Copy new key
			memmove(this->key, user_key, sizeof(user_key));
		}

		char* encrypt(char *content){
			bool success = false;
			// Attempt to encrypt content using key
			//
			// Create Cipher Text
			//
			memmove(this->iv, content, sizeof(this->iv));
			string ciphertext;
			CryptoPP::AES::Encryption aesEncryption(this->key, CryptoPP::AES::DEFAULT_KEYLENGTH);
			CryptoPP::CBC_Mode_ExternalCipher::Encryption cbcEncryption( aesEncryption, this->iv );

			CryptoPP::StreamTransformationFilter stfEncryptor(cbcEncryption, new CryptoPP::StringSink( ciphertext ) );
			//stfEncryptor.Put( reinterpret_cast<const unsigned char*>( plaintext.c_str() ), plaintext.length() + 1 );
			stfEncryptor.Put( reinterpret_cast<const unsigned char*>( content ), strlen(content) + 1 );
			stfEncryptor.MessageEnd();

			//return reinterpret_cast<const char*>(ciphertext.c_str());
			return (char*)ciphertext.c_str();
		}

		char* decrypt(char* content){
			// Attempt to decrypt content using key
			//
			// Decrypt
			//
			memmove(this->iv, content, sizeof(this->iv));
			content = content + sizeof(this->iv);
			string decryptedtext;
			CryptoPP::AES::Decryption aesDecryption(this->key, CryptoPP::AES::DEFAULT_KEYLENGTH);
			CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcDecryption( aesDecryption, this->iv );

			CryptoPP::StreamTransformationFilter stfDecryptor(cbcDecryption, new CryptoPP::StringSink( decryptedtext ) );
			stfDecryptor.Put( reinterpret_cast<const unsigned char*>( content ), strlen(content));
			stfDecryptor.MessageEnd();


			//return reinterpret_cast<const char*>(decryptedtext.c_str());
			return (char*)decryptedtext.c_str();
		}
};

extern "C" {
	AESCipher* aes(char* user_key) { return new AESCipher(user_key); }

	char* AES_Encrypt(AESCipher* cipher, char* content) { 
		return cipher->encrypt(content); 
	}

	char* AES_Decrypt(AESCipher* cipher, char* content) { 
		return cipher->decrypt(content); 
	}
}

int main(int argc, char* argv[]) {

    //
    // String and Sink setup
    //
    string plaintext = "this is just added to make this really long. Now is the time for all good men to come to the aide...";
    string ciphertext;
    string decryptedtext;

    //
    // Dump Plain Text
    //
    cout << "Plain Text (" << plaintext.size() << " bytes)" << endl;
    cout << plaintext;
    cout << endl << endl;


    //
    // Dump Decrypted Text
    //
    cout << "Decrypted Text: " << endl;
    cout << decryptedtext;
    cout << endl << endl;

    return 0;
}
