from ctypes import cdll
from ctypes import c_char_p
lib = cdll.LoadLibrary('./aes.so')
lib.AES_Encrypt.restype = c_char_p
lib.AES_Decrypt.restype = c_char_p


class AESCipher(object):
	def __init__(self, key=None):
		self.key = key or "key"
		self.obj = lib.aes(key)
	
	def encrypt(self, content):
		#lib.AES_Encrypt(self.obj, self.key)
		return lib.AES_Encrypt(self.obj, content)

	def decrypt(self, content):
		return lib.AES_Decrypt(self.obj, content)

if __name__ == "__main__":
    aes = AESCipher("key1")
    encrypted =  aes.encrypt("Hello World")
    print aes.decrypt(encrypted)
